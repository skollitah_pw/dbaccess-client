Program sklada sie z wielu klas oraz plikow konfiguracyjnych, dlatego prosta kompilacja poleceniem javac
jest niewygodna. Przygotowalem sie do cwiczenia i wiekszosc pracy wykonalem jeszcze w lipcu. Program korzysta z narzedzia Gradle i jest budowany jednym prostym
poleceniem. Nie potrzeba zadnych dodatkowych narzedzi poza Java 8.

Program jest klientem umozliwiajacym modyfikacje bazy danych. Posiada podstawowy, dość intuicyjny
interfejs

Wszystkie poniższe polecenia należy uruchamiać w katalogu ze zrodlami

Budowa aplikacji Windows
```sh
gradlew build
```

Budowa aplikacji Linux
```sh
./gradlew build
```

Uruchamianie aplikacji. Żeby aplikacja działała, najpierw nalezy
uruchomic serwer
```sh
java -jar build/libs/dbaccess-client-0.0.1-SNAPSHOT.jar
```

Aplikacja szuka serwera domyślnie pod adresem.
```
http://localhost:8080
```