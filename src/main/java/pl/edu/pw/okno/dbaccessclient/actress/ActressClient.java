package pl.edu.pw.okno.dbaccessclient.actress;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@FeignClient(value = "ActressClient", url = "${dbaccess.server.url}/api/actresses")
public interface ActressClient {

  @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  List<Actress> findAll();

  @GetMapping(value = "/{actress_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  Optional<Actress> findById(@PathVariable("actress_id") Integer actressId);

  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  Actress save(@RequestBody ActressForm actressForm);

  @PutMapping(
      value = "/{actress_id}",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  Actress update(
      @PathVariable("actress_id") Integer actressId, @RequestBody ActressForm actressForm);

  @DeleteMapping(value = "/{actress_id}")
  void delete(@PathVariable("actress_id") Integer actressId);
}
