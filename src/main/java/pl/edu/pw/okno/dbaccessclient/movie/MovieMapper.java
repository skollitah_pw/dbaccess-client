package pl.edu.pw.okno.dbaccessclient.movie;

import org.springframework.stereotype.Component;

@Component
public class MovieMapper {

  public MovieForm map(Movie movie) {
    return MovieForm.builder()
        .title(movie.getTitle())
        .year(movie.getYear())
        .actressName(movie.getActress().getName())
        .directorName(movie.getDirector().getName())
        .build();
  }
}
