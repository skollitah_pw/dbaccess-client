package pl.edu.pw.okno.dbaccessclient.actress;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import java.util.stream.Collectors;

import static pl.edu.pw.okno.dbaccessclient.ArgumentValidator.checkAttributesArray;

@RequiredArgsConstructor
@Component
public class ActressOperation implements DataOperation {

  private final ActressClient actressClient;

  @Override
  public Pair<String[], String[][]> retrieve() {
    String[][] values =
        actressClient.findAll().stream()
            .map(
                actress ->
                    new String[] {
                      actress.getId().toString(),
                      actress.getName(),
                      Integer.valueOf(actress.getYearOfBirth()).toString()
                    })
            .collect(Collectors.toList())
            .toArray(new String[][] {});
    String[] headers = new String[] {"Id", "Nazwisko", "Rok Urodzenia"};

    return Pair.of(headers, values);
  }

  @Override
  public void save(String[] data) {
    checkAttributesArray(data, 2);
    ActressForm form = ActressForm.builder().name(data[0]).yearOfBirth(Integer.parseInt(data[1])).build();
    actressClient.save(form);
  }

  @Override
  public void update(Integer id, String[] data) {
    checkAttributesArray(data, 2);
    ActressForm form = ActressForm.builder().name(data[0]).yearOfBirth(Integer.parseInt(data[1])).build();
    actressClient.update(id, form);
  }

  @Override
  public void delete(Integer id) {
    actressClient.delete(id);
  }
}
