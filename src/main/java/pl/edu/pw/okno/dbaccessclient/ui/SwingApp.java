package pl.edu.pw.okno.dbaccessclient.ui;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.dbaccessclient.actress.ActressOperation;
import pl.edu.pw.okno.dbaccessclient.director.DirectorOperation;
import pl.edu.pw.okno.dbaccessclient.movie.MovieOperation;

import javax.annotation.PostConstruct;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.*;
import java.util.Arrays;

@Slf4j
@Component
public class SwingApp extends JFrame {

  private final ApplicationContext applicationContext;

  public SwingApp(ApplicationContext applicationContext) throws HeadlessException {
    super();
    this.applicationContext = applicationContext;
  }

  @PostConstruct
  public void init() {
    TableView actressPanel = new TableView(applicationContext.getBean(ActressOperation.class));
    TableView directorPanel = new TableView(applicationContext.getBean(DirectorOperation.class));
    TableView moviePanel = new TableView(applicationContext.getBean(MovieOperation.class));

    JTabbedPane tabs = new JTabbedPane();
    tabs.add("Aktorka", actressPanel);
    tabs.add("Rezyser", directorPanel);
    tabs.add("Film", moviePanel);

    createLayout(tabs);
    setTitle("Database Editor");
    setSize(700, 400);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  private void createLayout(JComponent... arg) {

    Container pane = getContentPane();
    FlowLayout gl = new FlowLayout();
    pane.setLayout(gl);

    Arrays.stream(arg).forEach(pane::add);
  }
}
