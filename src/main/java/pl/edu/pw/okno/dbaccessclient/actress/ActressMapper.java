package pl.edu.pw.okno.dbaccessclient.actress;

import org.springframework.stereotype.Component;

@Component
public class ActressMapper {

  public ActressForm map(Actress actress) {
    return ActressForm.builder()
        .name(actress.getName())
        .yearOfBirth(actress.getYearOfBirth())
        .build();
  }
}
