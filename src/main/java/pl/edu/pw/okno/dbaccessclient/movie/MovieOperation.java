package pl.edu.pw.okno.dbaccessclient.movie;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import java.util.stream.Collectors;

import static pl.edu.pw.okno.dbaccessclient.ArgumentValidator.checkAttributesArray;

@RequiredArgsConstructor
@Component
public class MovieOperation implements DataOperation {

  private final MovieClient movieClient;

  @Override
  public Pair<String[], String[][]> retrieve() {
    String[][] values =
        movieClient.findAll().stream()
            .map(
                movie ->
                    new String[] {
                      movie.getId().toString(),
                      movie.getTitle(),
                      Integer.valueOf(movie.getYear()).toString(),
                      movie.getDirector().getName(),
                      movie.getActress().getName()
                    })
            .collect(Collectors.toList())
            .toArray(new String[][] {});
    String[] headers = new String[] {"Id", "Tytul", "Rok Produkcji", "Rezyser", "Aktorka"};

    return Pair.of(headers, values);
  }

  @Override
  public void save(String[] data) {
    checkAttributesArray(data, 4);
    MovieForm form =
        MovieForm.builder()
            .title(data[0])
            .year(Integer.parseInt(data[1]))
            .directorName(data[2])
            .actressName(data[3])
            .build();
    movieClient.save(form);
  }

  @Override
  public void update(Integer id, String[] data) {
    checkAttributesArray(data, 4);
    MovieForm form =
        MovieForm.builder()
            .title(data[0])
            .year(Integer.parseInt(data[1]))
            .directorName(data[2])
            .actressName(data[3])
            .build();

    movieClient.update(id, form);
  }

  @Override
  public void delete(Integer id) {
    movieClient.delete(id);
  }
}
