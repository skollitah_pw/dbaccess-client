package pl.edu.pw.okno.dbaccessclient.movie;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.edu.pw.okno.dbaccessclient.actress.Actress;
import pl.edu.pw.okno.dbaccessclient.director.Director;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Movie {

  private Integer id;
  private String title;
  private int year;
  private Actress actress;
  private Director director;
}
