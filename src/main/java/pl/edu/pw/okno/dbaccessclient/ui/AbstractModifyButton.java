package pl.edu.pw.okno.dbaccessclient.ui;

import com.google.common.base.Strings;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Arrays;

import static javax.swing.JOptionPane.showMessageDialog;

public abstract class AbstractModifyButton extends JButton {

  private final JTable dataTable;

  public AbstractModifyButton(String text, JTable dataTable) {
    super(text);
    this.dataTable = dataTable;
  }

  protected abstract void callUpdate(JTextField[] textFields);

  protected abstract void process();

  protected void init() {
    this.addActionListener((ActionEvent e) -> process());
  }

  protected JTextField[] buildDialog() {
    JFrame dialog = new JFrame();
    dialog.setSize(250, 400);
    dialog.setTitle("Insert Record");
    int numberOfAttributes = dataTable.getColumnCount() - 1;
    BorderLayout layout = new BorderLayout();
    dialog.setLayout(layout);
    JTextField[] attributeTextFields = new JTextField[numberOfAttributes];
    JPanel dataPanel = new JPanel();
    dataPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    for (int i = 0; i < numberOfAttributes; i++) {
      JPanel attributePanel = new JPanel();
      attributePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
      attributePanel.add(new JLabel(dataTable.getColumnName(i + 1)));
      JTextField textField = new JTextField();
      textField.setPreferredSize(new Dimension(100, 20));
      attributeTextFields[i] = textField;
      attributePanel.add(attributeTextFields[i]);
      dataPanel.add(attributePanel);
    }
    dialog.add(dataPanel, BorderLayout.CENTER);

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());

    JButton addButton = createSaveButton(dialog, attributeTextFields);
    JButton cancelButton = createCancelButton(dialog);
    buttonPanel.add(addButton);
    buttonPanel.add(cancelButton);
    dialog.add(buttonPanel, BorderLayout.SOUTH);
    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    dialog.setVisible(true);

    return attributeTextFields;
  }

  private JButton createSaveButton(JFrame dialog, JTextField[] textFields) {
    JButton saveButton = new JButton("Save");
    saveButton.addActionListener(
        (ActionEvent e) -> {
          try {
            callUpdate(textFields);
            dialog.dispose();
          } catch (Exception exception) {
            showMessageDialog(this, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
          }
        });

    return saveButton;
  }

  private JButton createCancelButton(JFrame dialog) {
    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener((ActionEvent e) -> dialog.dispose());
    return cancelButton;
  }

  protected boolean checkValues(String[] attributes) {
    return Arrays.stream(attributes).anyMatch(Strings::isNullOrEmpty);
  }
}
