package pl.edu.pw.okno.dbaccessclient;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import pl.edu.pw.okno.dbaccessclient.ui.SwingApp;

import java.awt.EventQueue;

@SpringBootApplication
@EnableFeignClients
public class DbaccessClientApplication {

  public static void main(String[] args) {
    ConfigurableApplicationContext ctx =
        new SpringApplicationBuilder(DbaccessClientApplication.class).headless(false).run(args);

    EventQueue.invokeLater(
        () -> {
          SwingApp ex = ctx.getBean(SwingApp.class);
          ex.setVisible(true);
        });
  }
}
