package pl.edu.pw.okno.dbaccessclient.director;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@FeignClient(value = "DirectorClient", url = "${dbaccess.server.url}/api/directors")
public interface DirectorClient {

  @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  List<Director> findAll();

  @GetMapping(value = "/{director_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  Optional<Director> findById(@PathVariable("director_id") Integer directorId);

  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  Director save(@RequestBody DirectorForm directorForm);

  @PutMapping(
      value = "/{director_id}",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  Director update(
      @PathVariable("director_id") Integer directorId, @RequestBody DirectorForm directorForm);

  @DeleteMapping(value = "/{director_id}")
  void delete(@PathVariable("director_id") Integer directorId);
}
