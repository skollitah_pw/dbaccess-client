package pl.edu.pw.okno.dbaccessclient.ui;

import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@Slf4j
public class TableView extends JPanel {

  private final DataOperation dataOperation;

  private String[][] data;
  private String[] headers;

  public TableView(DataOperation dataOperation) {
    this.dataOperation = dataOperation;
    init();
  }

  public void init() {
    Pair<String[], String[][]> input = dataOperation.retrieve();
    headers = input.getFirst();
    data = input.getSecond();
    BorderLayout firstLevelLayout = new BorderLayout();
    setLayout(firstLevelLayout);
    TableModel tableModel = new IdNotEditableModel(data, headers);
    JTable dataTable = new JTable(tableModel);
    dataTable.setBounds(30, 40, 200, 300);
    JScrollPane tableScrollPane = new JScrollPane(dataTable);
    tableScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    tableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    add(tableScrollPane, BorderLayout.CENTER);

    JPanel eastPanel = new JPanel();
    BoxLayout eastPaneLayout = new BoxLayout(eastPanel, BoxLayout.Y_AXIS);
    eastPanel.setLayout(eastPaneLayout);
    add(eastPanel, BorderLayout.EAST);

    JButton editButton = new UpdateButton("Edit", dataTable, dataOperation);
    JButton insertButton = new InsertButton("Insert", dataTable, dataOperation);
    JButton deleteButton = new DeleteButton("Delete", dataTable, dataOperation);

    eastPanel.add(editButton);
    eastPanel.add(insertButton);
    eastPanel.add(deleteButton);
  }

  private static class IdNotEditableModel extends DefaultTableModel {

    public IdNotEditableModel(Object[][] data, Object[] columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      return false;
    }
  }
}
