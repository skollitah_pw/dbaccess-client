package pl.edu.pw.okno.dbaccessclient.ui;

import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;

import static javax.swing.JOptionPane.showMessageDialog;

public class DeleteButton extends JButton {

  private final JTable dataTable;
  private final DataOperation dataOperation;

  public DeleteButton(String text, JTable dataTable, DataOperation dataOperation) {
    super(text);
    this.dataTable = dataTable;
    this.dataOperation = dataOperation;
    init();
  }

  private void init() {
    this.addActionListener((ActionEvent event) -> callDelete());
  }

  private void callDelete() {
    int selectedRow = dataTable.getSelectedRow();

    if (selectedRow < 0) {
      showMessageDialog(
          this.getRootPane(),
          "Record not deleted. Please select table row before.",
          "Error",
          JOptionPane.ERROR_MESSAGE);
      return;
    }

    int id = Integer.parseInt((String) dataTable.getValueAt(selectedRow, 0));

    try {
      dataOperation.delete(id);
      Pair<String[], String[][]> input = dataOperation.retrieve();
      ((DefaultTableModel) dataTable.getModel()).setDataVector(input.getSecond(), input.getFirst());

    } catch (Exception exception) {
      showMessageDialog(this, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}
