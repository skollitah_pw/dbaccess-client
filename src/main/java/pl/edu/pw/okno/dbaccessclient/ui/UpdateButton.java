package pl.edu.pw.okno.dbaccessclient.ui;

import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.util.Arrays;
import java.util.stream.Collectors;

public class UpdateButton extends AbstractModifyButton {

  private final JTable dataTable;
  private final DataOperation dataOperation;

  public UpdateButton(String text, JTable dataTable, DataOperation dataOperation) {
    super(text, dataTable);
    this.dataTable = dataTable;
    this.dataOperation = dataOperation;
    init();
  }

  @Override
  protected void process() {
    JTextField[] attributeFields = buildDialog();
    fillTextFields(attributeFields);
  }

  private void fillTextFields(JTextField[] attributeFields) {
    int selectedRow = dataTable.getSelectedRow();
    for (int i = 0; i < dataTable.getColumnCount() - 1; i++) {
      attributeFields[i].setText((String) dataTable.getValueAt(selectedRow, i + 1));
    }
  }

  @Override
  protected void callUpdate(JTextField[] textFields) {
    String[] attributes =
        Arrays.stream(textFields)
            .map(JTextComponent::getText)
            .collect(Collectors.toList())
            .toArray(new String[textFields.length]);

    if (checkValues(attributes)) {
      throw new RuntimeException(
          "Record not updated. Empty values provided. Please set all values");
    }

    int selectedRow = dataTable.getSelectedRow();
    int id = Integer.parseInt((String) dataTable.getValueAt(selectedRow, 0));

    dataOperation.update(id, attributes);
    Pair<String[], String[][]> input = dataOperation.retrieve();
    ((DefaultTableModel) dataTable.getModel()).setDataVector(input.getSecond(), input.getFirst());
  }
}
