package pl.edu.pw.okno.dbaccessclient.movie;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@FeignClient(value = "MovieClient", url = "${dbaccess.server.url}/api/movies")
public interface MovieClient {

  @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  List<Movie> findAll();

  @GetMapping(value = "/{movie_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  Optional<Movie> findById(@PathVariable("movie_id") Integer movieId);

  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  Movie save(@RequestBody MovieForm movieForm);

  @PutMapping(
      value = "/{movie_id}",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  Movie update(@PathVariable("movie_id") Integer movieId, @RequestBody MovieForm movieForm);

  @DeleteMapping(value = "/{movie_id}")
  void delete(@PathVariable("movie_id") Integer movieId);
}
