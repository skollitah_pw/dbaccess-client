package pl.edu.pw.okno.dbaccessclient.ui;

import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.util.Arrays;
import java.util.stream.Collectors;

public class InsertButton extends AbstractModifyButton {

  private final JTable dataTable;
  private final DataOperation dataOperation;

  public InsertButton(String text, JTable dataTable, DataOperation dataOperation) {
    super(text, dataTable);
    this.dataTable = dataTable;
    this.dataOperation = dataOperation;
    init();
  }

  @Override
  protected void process() {
    buildDialog();
  }

  @Override
  protected void callUpdate(JTextField[] textFields) {
    String[] attributes =
        Arrays.stream(textFields)
            .map(JTextComponent::getText)
            .collect(Collectors.toList())
            .toArray(new String[textFields.length]);

    if (checkValues(attributes)) {
      throw new RuntimeException("Record not added. Empty values provided. Please set all values");
    }

    dataOperation.save(attributes);
    Pair<String[], String[][]> input = dataOperation.retrieve();
    ((DefaultTableModel) dataTable.getModel()).setDataVector(input.getSecond(), input.getFirst());
  }
}
