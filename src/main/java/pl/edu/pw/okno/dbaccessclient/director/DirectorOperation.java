package pl.edu.pw.okno.dbaccessclient.director;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.dbaccessclient.DataOperation;
import pl.edu.pw.okno.dbaccessclient.utils.Pair;

import java.util.stream.Collectors;

import static pl.edu.pw.okno.dbaccessclient.ArgumentValidator.checkAttributesArray;

@RequiredArgsConstructor
@Component
public class DirectorOperation implements DataOperation {

  private final DirectorClient directorClient;

  @Override
  public Pair<String[], String[][]> retrieve() {
    String[][] values =
        directorClient.findAll().stream()
            .map(
                director ->
                    new String[] {
                      director.getId().toString(),
                      director.getName(),
                      Integer.valueOf(director.getYearOfBirth()).toString()
                    })
            .collect(Collectors.toList())
            .toArray(new String[][] {});
    String[] headers = new String[] {"Id", "Nazwisko", "Rok Urodzenia"};

    return Pair.of(headers, values);
  }

  @Override
  public void save(String[] data) {
    checkAttributesArray(data, 2);
    DirectorForm form = DirectorForm.builder().name(data[0]).yearOfBirth(Integer.parseInt(data[1])).build();
    directorClient.save(form);
  }

  @Override
  public void update(Integer id, String[] data) {
    checkAttributesArray(data, 2);
    DirectorForm form = DirectorForm.builder().name(data[0]).yearOfBirth(Integer.parseInt(data[1])).build();
    directorClient.update(id, form);
  }

  @Override
  public void delete(Integer id) {
    directorClient.delete(id);
  }
}
