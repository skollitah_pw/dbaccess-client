package pl.edu.pw.okno.dbaccessclient;

import pl.edu.pw.okno.dbaccessclient.utils.Pair;

public interface DataOperation {

  Pair<String[], String[][]> retrieve();

  void save(String[] data);

  void update(Integer id, String[] data);

  void delete(Integer id);
}
