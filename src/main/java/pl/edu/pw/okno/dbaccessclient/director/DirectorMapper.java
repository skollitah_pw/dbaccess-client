package pl.edu.pw.okno.dbaccessclient.director;

import org.springframework.stereotype.Component;

@Component
public class DirectorMapper {

  public DirectorForm map(Director director) {
    return DirectorForm.builder()
        .name(director.getName())
        .yearOfBirth(director.getYearOfBirth())
        .build();
  }
}
