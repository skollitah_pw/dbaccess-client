package pl.edu.pw.okno.dbaccessclient;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.google.common.base.Preconditions.checkArgument;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ArgumentValidator {

  public static void checkAttributesArray(String[] data, int expectedLength) {
    checkArgument(data != null && data.length == expectedLength, "Invalid table attributes array");
  }
}
